package register

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/acoshift/hrpc"
)

type errorResponse struct {
	Error string `json:"error"`
}

var (
	errMethodNotAllowed = errors.New("invoice: method not allowed")
)

// MakeHandler for ticket domain
func MakeHandler(s Service) http.Handler {

	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}
	mux := http.NewServeMux()
	mux.Handle("/create", m.Handler(MakeCreateRegister(s)))
	mux.Handle("/update", m.Handler(MakeUpdateRegister(s)))
	mux.Handle("/read", m.Handler(MakeGetRegisterID(s)))
	mux.Handle("/reads", m.Handler(MakeGetRegisterAll(s)))
	mux.Handle("/del", m.Handler(MakeDelRegisterID(s)))
	return (mux)
}

func jsonDecoder(r *http.Request, v interface{}) error {
	return json.NewDecoder(r.Body).Decode(v)
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	fmt.Println("v =", v)
	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder

	var status = http.StatusOK

	fmt.Println("Error Encode = ", err.Error())
	switch err.Error() {

	default:
		status = http.StatusOK
	}

	encoder(w, status, &errorResponse{err.Error()})
}

func requestDecoder(r *http.Request, v interface{}) error {
	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	fmt.Println("v =", r)
	return jsonDecoder(r, v)
}
