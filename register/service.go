package register

// Service inter
type Service interface {
	// // ListTRegisterService(keyword string, page, limit int64) (int64, interface{}, error)
	CreateRegisterService(model *MemberModel, userCode string) (interface{}, error)
	UpdateRegisterService(model *MemberModel, userCode string) (interface{}, error)
	GetRegisterService(ID string) (interface{}, error)
	DelRegisterService(model *MemberModel) (interface{}, error)
	GetRegisterAllService(ID string) ([]MemberModel, error)
	// ListTRegister(id int64) ([]TMemberModel, error)
	// ListGroup(companyID int64) (interface{}, error)
	// GetProject(id int64) (interface{}, error)
}
type service struct {
	repo Repository
}

// NewService creates new auth service
func NewService(repo Repository) (Service, error) {
	s := service{repo}
	return &s, nil
}

func (s *service) CreateRegisterService(model *MemberModel, userCode string) (interface{}, error) {
	_, err := s.repo.CreateRegisterRepo(model, userCode)
	if err != nil {
		return nil, err
	}
	return nil, nil
}
func (s *service) UpdateRegisterService(model *MemberModel, userCode string) (interface{}, error) {
	_, err := s.repo.UpdateRegisterRepo(model)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (s *service) GetRegisterService(ID string) (interface{}, error) {
	resp, err := s.repo.GetRegisterRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) GetRegisterAllService(ID string) ([]MemberModel, error) {
	resp, err := s.repo.GetRegisterAllRepo(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (s *service) DelRegisterService(model *MemberModel) (interface{}, error) {
	resp, err := s.repo.DelRegisterRepo(model)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
