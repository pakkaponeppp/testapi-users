package register

import (
	"context"
	"fmt"
)

//MakeCreateRegister DEE
func MakeCreateRegister(s Service) interface{} {
	type request MemberModel
	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		fmt.Println("begin endpoint.makecreateRegister")
		model := MemberModel{
			ID:        req.ID,
			Username:  req.Username,
			Password:  req.Password,
			Firstname: req.Firstname,
			Lastname:  req.Lastname,
			Phone:     req.Phone,
		}

		resp, err := s.CreateRegisterService(&model, "")

		if err != nil {
			fmt.Println("makecreateRegister error ", err.Error())
			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "สมัครสมาชิกสำเร็จ",
			Data:    resp,
		}, nil
	}
}

//MakeUpdateRegister DEE
func MakeUpdateRegister(s Service) interface{} {
	type request MemberModel
	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		fmt.Println("begin endpoint.makecreateRegister")
		model := MemberModel{
			ID:        req.ID,
			Username:  req.Username,
			Password:  req.Password,
			Firstname: req.Firstname,
			Lastname:  req.Lastname,
			Phone:     req.Phone,
		}

		resp, err := s.UpdateRegisterService(&model, "")

		if err != nil {
			fmt.Println("makeupdateRegister error ", err.Error())
			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "แก้ไขข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}

//MakeGetRegisterID ddd
func MakeGetRegisterID(s Service) interface{} {
	type request struct {
		ID string `json:"id"`
	}
	type response struct {
		Data interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		resp, err := s.GetRegisterService(req.ID)
		if err != nil {
			fmt.Println("makeSigninEndpoint error ", err.Error())

		}
		return &response{

			Data: resp,
		}, nil
	}
}

//MakeGetRegisterAll ddd
func MakeGetRegisterAll(s Service) interface{} {
	type request struct {
		ID string `json:"id"`
	}
	type response struct {
		Data interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		resp, err := s.GetRegisterAllService(req.ID)
		if err != nil {
			fmt.Println("makeSigninEndpoint error ", err.Error())

		}
		return &response{

			Data: resp,
		}, nil
	}
}

//MakeDelRegisterID ddd
func MakeDelRegisterID(s Service) interface{} {
	type request struct {
		ID string `json:"id"`
	}
	type response struct {
		Result  string      `json:"response"`
		Message string      `json:"message"`
		Data    interface{} `json:"data"`
	}
	return func(ctx context.Context, req *request) (*response, error) {
		fmt.Println("begin endpoint.makecreateRegister")
		model := MemberModel{
			ID: req.ID,
		}

		resp, err := s.DelRegisterService(&model)

		if err != nil {
			fmt.Println("makeupdateRegister error ", err.Error())
			return &response{Result: "false", Message: err.Error()}, nil
		}
		return &response{
			Result:  "success",
			Message: "ลบข้อมูลสำเร็จ",
			Data:    resp,
		}, nil
	}
}
