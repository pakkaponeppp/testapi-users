package register

// Repository ok
type Repository interface {
	UpdateRegisterRepo(req *MemberModel) (interface{}, error)
	CreateRegisterRepo(model *MemberModel, usercode string) (interface{}, error)
	GetRegisterRepo(ID string) (interface{}, error)
	DelRegisterRepo(req *MemberModel) (interface{}, error)
	GetRegisterAllRepo(ID string) ([]MemberModel, error)
}
