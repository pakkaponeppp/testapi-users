package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/acoshift/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/pakkaponeppp/SIGN/auth"
	mysqldb "github.com/pakkaponeppp/SIGN/mysql"
	"github.com/pakkaponeppp/SIGN/register"
)

var (
	mysql   *sqlx.DB
	mode    = "dev"
	dbHost  = "192.168.0.70"
	dbUser  = "intern"
	dbName  = "intern"
	dbPort  = "3306"
	dbPass  = "intern"
	appPort = "3000"
)

// ConnectDB : Connect to production db
func ConnectDB(user string, pass string, dbName string, host string, port string) (db *sqlx.DB, err error) {
	dsn := user + ":" + pass + "@tcp(" + host + ":" + port + ")/" + dbName + "?parseTime=true&loc=Asia%2FBangkok&charset=utf8"
	fmt.Println(dsn, "DBName =", host, dbName)
	db, err = sqlx.Connect("mysql", dsn)
	if err != nil {
		fmt.Println("sql error =", err)
		return nil, err
	}
	db.Exec("use " + dbName)
	//db.Close()
	return db, err
}

func main() {

	mysqlCon, _ := ConnectDB(dbUser, dbPass, dbName, dbHost, dbPort)

	mysql = mysqlCon
	registerRepo := mysqldb.NewRegisterRepository(mysql)
	registerService, _ := register.NewService(registerRepo)
	mux := http.NewServeMux()
	mux.Handle("/register/", http.StripPrefix("/register/v1", register.MakeHandler(registerService)))
	corsConfig := middleware.CORSConfig{
		AllowAllOrigins: true,
		AllowMethods: []string{
			http.MethodGet,
			http.MethodPost,
			http.MethodPut,
			http.MethodPatch,
			http.MethodDelete,
			http.MethodOptions,
			http.MethodHead,
		},
		AllowHeaders: []string{
			"Content-Type",
			"Authorization",
			"Token",
			"Access-Token",
			"X-Access-Token",
			"Access-Control-Allow-Origin",
			"Access-Control-Allow-Headers",
			"Accept",
			"Content-Length",
			"Accept-Encoding",
			"X-CSRF-Token",
			"Origin",
			"X-Requested-With",
		},
		MaxAge: time.Hour,
	}

	h := auth.MakeMiddleware()(mux)

	h = middleware.CORS(corsConfig)(h)
	h = middleware.AddHeader("Content-Type", "application/json; charset=utf-8")(h)

	log.Println(appPort)

	http.ListenAndServe(":"+appPort, h)
}

func mainpage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Println("/home")
	w.WriteHeader(http.StatusOK)
}
