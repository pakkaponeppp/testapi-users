package auth

import "context"

type service struct {
	auths Repository
}
type (
	keyToken struct{}
)

type Service interface {
	// GetToken(tokenID string) (*AUTHMODEL, error)
	// CreateUserService(user *ModelUSERS, userCode string) (interface{}, error)
	// SigninService(username string, password string, projectID int64) (interface{}, error)
	// GETUSERLOGINSERVICE(auth *AUTHMODEL) (interface{}, error)
	// CREATEPROJECT(project *ModelProject, auth *AUTHMODEL) (interface{}, error)
	// GETPROJECTLISTSERIVCE(auth *AUTHMODEL) (interface{}, error)
	// AUTHPROJECTSERVICE(auth *AUTHMODEL, projectID int64) (interface{}, error)
	// GetUserCodes(userCode string) (interface{}, error)
	// ListUser(keyword string, page, limit int64, user *AUTHMODEL) (int64, interface{}, error)
	// UpdateUSerService(user *ModelUSERS, au *AUTHMODEL) (interface{}, error)
	// GETPROJECT(projectID int64) (interface{}, error)
	// GETDASHBOARD() (interface{}, error)
}

// NewService service
func NewService(repo Repository) (Service, error) {
	s := service{repo}
	return &s, nil
}

func GETAUTH(ctx context.Context) *AUTHMODEL {
	x, ok := ctx.Value(keyToken{}).(*AUTHMODEL)
	if !ok {
		return nil
	}
	return x
}

// GetUserCode returns account id from context
func GetUserCode(ctx context.Context) string {
	x, ok := ctx.Value(keyToken{}).(*AUTHMODEL)
	if !ok {
		return ""
	}
	return x.UserCode
}
