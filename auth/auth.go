package auth

// AUTHMODEL struct
type AUTHMODEL struct {
	TokenID   string
	UserCode  string
	Username  string
	Password  string
	Firstname string
	Lastname  string
	Phone     string
}

// Repository interface
type Repository interface {
	// GetToken(tokenID string) (*AUTHMODEL, error)
	// GETUSERUSERNAME(username string) (*ModelUSERS, error)
	// SaveTokenLogin(userCode string, token string,) (interface{}, error)
	// GETUSERCODEREPO(user *AUTHMODEL) (interface{}, error)
	// CreateUser(user *ModelUSERS, userCode string) (interface{}, error)
	// GETUSERCODE(userCode string) (*ModelUSERS, error)
}
