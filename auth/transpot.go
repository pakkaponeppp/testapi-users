package auth

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/acoshift/hrpc"
)

var (
	errMethodNotAllowed = errors.New("auth: method not allowed")
	errForbidden        = errors.New("auth: forbidden")
	errMultiToken       = errors.New("auth: MultiToken")
	errBadRequest       = errors.New("auth: bad request body")
	errUnauthorized     = errors.New("Unauthorized")
)

type errorResponse struct {
	Error string `json:"error"`
}

// MakeMiddleware creates new auth middleware
func MakeMiddleware() func(http.Handler) http.Handler {

	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			tokenID := r.Header.Get("Access-Token") //ERP
			if len(tokenID) == 0 {
				h.ServeHTTP(w, r)
				return
			}
			fmt.Println("auth.transport token DT: ", tokenID)
			tk := AUTHMODEL{}

			ctx := r.Context()
			ctx = context.WithValue(ctx, keyToken{}, tk)
			r = r.WithContext(ctx)
			h.ServeHTTP(w, r)

		})
	}
}

//MakeHandler sss
func MakeHandler(s Service) http.Handler {

	m := hrpc.Manager{
		Validate:     true,
		Decoder:      requestDecoder,
		Encoder:      responseEncoder,
		ErrorEncoder: errorEncoder,
	}

	mux := http.NewServeMux()
	// mux.Handle("/signin", m.Handler(makeSigninEndpoint(s)))
	// mux.Handle("/", m.Handler(MakeAccountHandlerErp(s, &m)))

	mux.Handle("/", m.Handler(MakeAccountHandler(s, &m)))
	return mux
}

// MakeAccountHandler creates new account handler
func MakeAccountHandler(s Service, m *hrpc.Manager) http.Handler {

	mux := http.NewServeMux()
	// mux.Handle("/get/user/login", m.Handler(makeGetUserLoginEndpoint(s)))
	// mux.Handle("/create/project", m.Handler(makeCreateProject(s)))
	// mux.Handle("/project/list", m.Handler(makelistProject(s)))
	// mux.Handle("/auth/project", m.Handler(makeAuthProject(s)))
	// mux.Handle("/create/user", m.Handler(MakeAddUserEndpoint(s)))

	// mux.Handle("/get/user", m.Handler(makeGetuserCode(s)))
	// mux.Handle("/update/user", m.Handler(makeUpdateUSer(s)))

	// mux.Handle("/get/project/id", m.Handler(makeGetPRoject(s)))
	// mux.Handle("/list/role", m.Handler(makeListRole(s)))
	// // mux.Handle("/get/menu/role", m.Handler(makeGetMenuRolEndpoint(s)))
	// mux.Handle("/create/menu", m.Handler(makecreateMenu(s)))
	// mux.Handle("/create/role", m.Handler(makecreateRolePermision(s)))
	// mux.Handle("/create/sp/permission", m.Handler(makeSPPermision(s)))
	// mux.Handle("/update/role", m.Handler(makeUpdateRolePermision(s)))
	// mux.Handle("/get/dashboard", m.Handler(makeGetDashboard(s)))
	// mux.Handle("/list/user", m.Handler(makeuserList(s)))
	// mux.Handle("/get/menu/role", m.Handler(makeGetMenuByRole(s)))
	return mustLogin(s)(mux)
}

func mustLogin(s Service) func(http.Handler) http.Handler {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			user := GetUserCode(r.Context())
			if len(user) == 0 {
				// errorEncoder(w, r, ErrokenNotFound)
				fmt.Println("error mustLogin auth.transport.go")
				return

			}
			enableCors(&w)
			h.ServeHTTP(w, r)

		})
	}
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Content-Type", "application/json")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set(
		"Access-Control-Allow-Headers",
		"Accept, Access-Token,X-Access-Token, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, access-control-allow-origin",
	)
}

func jsonDecoder(r *http.Request, v interface{}) error {
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		log.Printf("[Auth] API request: %+v\n", v)

		return errBadRequest
	}
	return nil
}

func jsonEncoder(w http.ResponseWriter, status int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type,Token,Access-Token,x-access-token")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.WriteHeader(status)
	log.Printf("[Auth] API response %d: %+v\n", status, v)
	return json.NewEncoder(w).Encode(v)
}

func requestDecoder(r *http.Request, v interface{}) error {
	if r.Method != http.MethodPost {
		return errMethodNotAllowed
	}
	return jsonDecoder(r, v)
}

func responseEncoder(w http.ResponseWriter, r *http.Request, v interface{}) {
	jsonEncoder(w, http.StatusOK, v)
}

func errorEncoder(w http.ResponseWriter, r *http.Request, err error) {
	encoder := jsonEncoder
	status := http.StatusInternalServerError
	fmt.Println("auth transport ,check error : ", err.Error())
	if err == errUnauthorized {
		fmt.Println("year equal to ...")
	} else {
		fmt.Println("not ...")
	}

	if r.Method == http.MethodOptions {
		encoder(w, http.StatusNoContent, nil)
	} else {
		encoder(w, status, &errorResponse{err.Error()})
	}
}
