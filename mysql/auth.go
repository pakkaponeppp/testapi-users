package mysqldb

import (
	"github.com/jmoiron/sqlx"
	"github.com/pakkaponeppp/SIGN/auth"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

type authRepository struct {
	db  sqlbuilder.Database
	dbx *sqlx.DB
}

//NewAuthRepository database
func NewAuthRepository(Bdb *sqlx.DB) auth.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}

	// fmt.Println(1)
	r := authRepository{bidb, Bdb}
	// fmt.Println(1)
	return &r
}
