package mysqldb

import (
	"log"

	"github.com/jmoiron/sqlx"
	"github.com/pakkaponeppp/SIGN/register"
	"upper.io/db.v3/lib/sqlbuilder"
	"upper.io/db.v3/mysql"
)

//NewRegisterRepository sss
func NewRegisterRepository(Bdb *sqlx.DB) register.Repository {

	bdb := Bdb.DB

	bidb, err := mysql.New(bdb)
	if err != nil {
		return nil
	}

	// fmt.Println(1)
	r := registerRepository{bidb, Bdb}
	// fmt.Println(1)
	return &r
}

type registerRepository struct {
	db  sqlbuilder.Database
	dbx *sqlx.DB
}

func (r *registerRepository) CreateRegisterRepo(model *register.MemberModel, usercode string) (interface{}, error) {
	_, err := r.CreateRegister(model, usercode)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (r *registerRepository) CreateRegister(model *register.MemberModel, usercode string) (interface{}, error) {

	q := r.db.
		InsertInto("USERS").
		Values(map[string]interface{}{
			"username":  model.Username,
			"password":  model.Password,
			"firstname": model.Firstname,
			"lastname":  model.Lastname,
			"phone":     model.Phone,
		})
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}

func (r *registerRepository) UpdateRegisterRepo(req *register.MemberModel) (interface{}, error) {
	resp, err := r.UpdateRegister(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *registerRepository) UpdateRegister(req *register.MemberModel) (interface{}, error) {

	q := r.db.Update("USERS").
		Set("password", req.Password).
		Set("firstname", req.Firstname).
		Set("lastname", req.Lastname).
		Set("phone", req.Phone).
		Where("id = ?", req.ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}
func (r *registerRepository) GetRegisterRepo(ID string) (interface{}, error) {
	resp, err := r.GetRegister(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *registerRepository) GetRegister(ID string) (*register.MemberModel, error) {
	sql1 := ` select a.id,
	ifnull(a.username,'') as username,
	ifnull(a.password,'') as password,
	ifnull(a.firstname,'') as firstname,
	ifnull(a.lastname,'') as lastname,
	ifnull(a.phone,'') as phone from USERS a where a.id = ?`
	rs := r.dbx.QueryRowx(sql1, ID)

	model := register.MemberModel{}
	err := rs.StructScan(&model)
	if err != nil {

		return nil, err
	}
	return &model, nil
}

func (r *registerRepository) GetRegisterAllRepo(ID string) ([]register.MemberModel, error) {
	resp, err := r.GetRegisterAll(ID)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
func (r *registerRepository) GetRegisterAll(ID string) ([]register.MemberModel, error) {
	sql1 := ` select a.id,
	ifnull(a.username,'') as username,
	ifnull(a.password,'') as password,
	ifnull(a.firstname,'') as firstname,
	ifnull(a.lastname,'') as lastname,
	ifnull(a.phone,'') as phone from USERS a `
	rs, err := r.dbx.Queryx(sql1)
	if err != nil {
		// log.Error(err.Error())
		return nil, err
	}
	models := []register.MemberModel{}
	for rs.Next() {
		model := register.MemberModel{}
		err = rs.StructScan(&model)
		if err != nil {
			// log.Error(err.Error())
			return nil, err
		}
		models = append(models, model)
	}
	return models, nil
}

func (r *registerRepository) DelRegisterRepo(req *register.MemberModel) (interface{}, error) {
	resp, err := r.DelRegister(req)
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (r *registerRepository) DelRegister(req *register.MemberModel) (interface{}, error) {

	q := r.db.DeleteFrom("USERS").
		Where("id = ?", req.ID)
	_, err := q.Exec()
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return nil, nil
}
